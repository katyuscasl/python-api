import pytest
from src.services.multiply_two_numbers import multiply_two_numbers


def test_multiply_with_zero_numbers():
  factor_1, factor_2 = 0, 0
  res = multiply_two_numbers(factor_1, factor_2)
  assert res == 0


def test_multiply_with_only_one_zero_number():
  factor_1, factor_2 = 10000, 0
  res = multiply_two_numbers(factor_1, factor_2)
  assert res == 0


def test_multiply_with_only_one_negative_number():
  factor_1, factor_2 = -10, 10
  res = multiply_two_numbers(factor_1, factor_2)
  assert res == -100
